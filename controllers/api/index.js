'use strict';

module.exports = function (router) {
    // automatically require all controllers in this folder
    require('fs').readdirSync(__dirname + '/').forEach(function (file) {
        if (file.match(/.+\.js$/g) !== null && file !== 'index.js') {
            // var name = file.replace('.js', '');
            require('./' + file)(router);
        }
    });
};
