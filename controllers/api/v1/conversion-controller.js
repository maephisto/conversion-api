'use strict';

const
    CurrencyConverter = require('../../../libs/currency-converter'),
    RatesProvider = require('../../../libs/rates-provider'),
    RatesStore = require('../../../libs/rates-store'),
    ECBRatesAPI = require('../../../libs/api/ecb-rates-api'),
    _ = require('underscore');

module.exports = function (router) {

    router.get('/convert', function (req, res) {

        try {
            if (!req.query || _.isEmpty(req.query)) {
                return res.status(400).json({
                    success: false,
                    message: 'Bad request! Query string parameters are missing'
                });
            }
            if (!req.query.from || !req.query.to || !req.query.amount) {
                return res.status(400).json({
                    success: false,
                    message: 'Query string parameters \'from\', \'to\', \'amount\' are mandatory'
                });
            }

            req.query.amount = parseFloat(req.query.amount);

            if (typeof req.query.amount !== 'number' || !isFinite(req.query.amount)) {
                return res.status(400).json({
                    success: false,
                    message: 'The amount is not a number'
                });
            }


            let ratesProvider = new RatesProvider(ECBRatesAPI, RatesStore);
            let converter = new CurrencyConverter(ratesProvider);
            let params = {
                from: req.query.from,
                to: req.query.to,
                amount: req.query.amount
            };
            converter.convert(params)
                .then((convertedAmount) => {
                    res.status(200).json({
                        convertedAmount: convertedAmount
                    });
                })
                .catch((err) => {
                    console.log('ERROR', err.message, err.stack);
                    res.status(500).json({
                        success: false,
                        message: err.message
                    });
                });
        } catch (error) {
            res.status(500).json({
                success: false,
                message: error.message
            });
        }


    });

};
