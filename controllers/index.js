'use strict';

module.exports = function (router) {

    router.get('/', function (req, res) {
        
        res.status(400).json({ message: 'This is an API! Check the documentation at http://somedocumentation.com :)' });
        
    });

};
