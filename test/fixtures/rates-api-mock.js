'use strict';

module.exports = {
    fetchRates: function () {
        return new Promise((resolve, reject) => {
            resolve({
                base: 'EUR',
                rates: {}
            });
        });
    }
};