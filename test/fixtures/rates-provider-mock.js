'use strict';

var RatesProviderMock = function () {};

RatesProviderMock.prototype.getRates = function () {
    return new Promise((resolve, reject) => {
        resolve({
            base: 'BASECURR',
            rates: {
                'CURR1': 1.0650,
                'CURR2': 0.8572
            }
        });
    });
};


module.exports = RatesProviderMock;
