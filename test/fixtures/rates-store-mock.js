'use strict';

module.exports = {

    setRates: function (data) {
        return new Promise((resolve, reject) => {
            this.ratesData = data;
            resolve(this.ratesData);
        });
    },

    fetchRates: function () {
        return new Promise((resolve, reject) => {
            resolve(this.ratesData || null);
        });
    },

    clear: function () {
        this.ratesData = null;
    }

};
