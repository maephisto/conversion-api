'use strict';


const
    CurrencyConverter = require('../../libs/currency-converter'),
    RatesProviderMock = require('../fixtures/rates-provider-mock');


const
    chai = require('chai'),
    spies = require('chai-spies'),
    _ = require('underscore'),
    expect = chai.expect;

chai.use(spies);

describe('Currency converter specification:', function () {

    before(function (done) {
        done();
    });

    it('should initialize properly', function (done) {
        let ratesProvider = new RatesProviderMock();
        let converter = new CurrencyConverter(ratesProvider);
        done();
    });

    it('should throw an exception if there is no rates provider injected', function (done) {
        try {
            let converter = new CurrencyConverter();
        } catch (error) {
            expect(error).to.be.ok;
            expect(error.message).to.equal('A Rates Provider, we need, mhmm!');
            done();
        }
    });

    it('should throw an exception if some conversion params are missing', function (done) {
        let ratesProvider = new RatesProviderMock();
        let converter = new CurrencyConverter(ratesProvider);
        let params = {
            to: 'USD',
            amount: 100
        };
        converter.convert(params)
            .catch((error) => {
                expect(error).to.be.ok;
                expect(error.message).to.equal('Some of the conversion parameters are missing!');
                done();
            });
    });

    it('should throw an exception if the conversion params are missing', function (done) {
        let ratesProvider = new RatesProviderMock();
        let converter = new CurrencyConverter(ratesProvider);

        converter.convert()
            .catch((error) => {
                expect(error).to.be.ok;
                expect(error.message).to.equal('Conversion parameters are missing!');
                done();
            });
    });

    it('should convert an amount of money to some other amount', function (done) {
        let ratesProvider = new RatesProviderMock();
        let converter = new CurrencyConverter(ratesProvider);
        let params = {
            from: 'CURR1',
            to: 'CURR2',
            amount: 100
        };
        converter.convert(params)
            .then((convertedAmount) => {
                expect(convertedAmount).to.be.ok;
                done();
            });
    });

    it('should fail to convert an amount of money from/to an inexistent currency', function (done) {
        let ratesProvider = new RatesProviderMock();
        let converter = new CurrencyConverter(ratesProvider);
        let params = {
            from: 'XXX',
            to: 'CURR2',
            amount: 100
        };
        converter.convert(params)
            .catch((error) => {
                expect(error).to.be.ok;
                expect(error.message).to.equal('One or both from/to currencies are invalid');
                done();
            });
    });

    it('should convert an amount of base currency to the CURR2 amount ', function (done) {
        let ratesProvider = new RatesProviderMock();
        let converter = new CurrencyConverter(ratesProvider);
        let params = {
            from: 'BASECURR',
            to: 'CURR1',
            amount: 100
        };
        converter.convert(params)
            .then((convertedAmount) => {
                expect(convertedAmount).to.be.ok;
                expect(convertedAmount).to.equal(106.5);
                done();
            });
    });

    it('should convert an amount of currency to the base currency', function (done) {
        let ratesProvider = new RatesProviderMock();
        let converter = new CurrencyConverter(ratesProvider);
        let params = {
            from: 'CURR1',
            to: 'BASECURR',
            amount: 100
        };
        converter.convert(params)
            .then((convertedAmount) => {
                expect(convertedAmount).to.be.ok;
                expect(convertedAmount).to.equal(93.8967);
                done();
            });
    });

    it('should convert an amount of currency CURR1 to the CURR2 amount ', function (done) {
        let ratesProvider = new RatesProviderMock();
        let converter = new CurrencyConverter(ratesProvider);
        let params = {
            from: 'CURR1',
            to: 'CURR2',
            amount: 100
        };
        converter.convert(params)
            .then((convertedAmount) => {
                expect(convertedAmount).to.be.ok;
                expect(convertedAmount).to.equal(80.4883);
                done();
            });
    });

    it('should round a value correctly', function (done) {
        let ratesProvider = new RatesProviderMock();
        let converter = new CurrencyConverter(ratesProvider);
        expect(converter.round(123.456789, 4)).to.equal(123.4568);
        done();
    });

    it('should fail to round a string value', function (done) {
        try {
            let ratesProvider = new RatesProviderMock();
            let converter = new CurrencyConverter(ratesProvider);
            converter.round('bbb123aaa');
        } catch (error) {
            expect(error).to.be.ok;
            expect(error.message).to.equal('Rounded amounts should be numbers, string provided!');
        } finally {
            done();
        }
    });

    it('should throw an exception when trying to convertAndRoundAmount without an amount or rate', function (done) {
        try {
            let ratesProvider = new RatesProviderMock();
            let converter = new CurrencyConverter(ratesProvider);
            converter.convertAndRoundAmount();
        } catch (error) {
            expect(error).to.be.ok;
            expect(error.message).to.equal('Missing amount or rate on conversion');
            done();
        }
    });

    it('should throw an exception when trying to convertAndRoundAmount with a bad amount or rate', function (done) {
        try {
            let ratesProvider = new RatesProviderMock();
            let converter = new CurrencyConverter(ratesProvider);
            converter.convertAndRoundAmount(100, 'haha');
        } catch (error) {
            expect(error).to.be.ok;
            expect(error.message).to.equal('Error converting/rounding values');
            done();
        }
        done();
    });

    it('should convertAndRoundAmount an amount correctly', function (done) {
        let ratesProvider = new RatesProviderMock();
        let converter = new CurrencyConverter(ratesProvider);
        expect(converter.convertAndRoundAmount(100, 1.0654)).to.equal(106.54);
        done();
    });
});
