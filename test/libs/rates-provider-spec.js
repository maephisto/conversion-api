'use strict';

const 
    RatesProvider = require('../../libs/rates-provider'),
    RatesAPIMock = require('../fixtures/rates-api-mock'),
    RatesStoreMock = require('../fixtures/rates-store-mock');

const
    chai = require('chai'),
    spies = require('chai-spies'),
    _ = require('underscore'),
    expect = chai.expect;

chai.use(spies);

describe('Rates provider specification:', function () {

    before(function (done) {
        done();
    });

    it('should initialize properly', function (done) {
        let ratesProvider = new RatesProvider({}, {});
        done();
    });

    it('should fail to init if dependencies are not provided', function (done) {
        try {
            let ratesProvider = new RatesProvider();
        } catch (error) {
            expect(error).to.be.ok;
            expect(error.message).to.equal('Missing dependecies!')
        } finally {
            done();
        }
    });

    it('should return a set of rates no matter what', function (done) {
        let ratesProvider = new RatesProvider(RatesAPIMock, RatesStoreMock);
        ratesProvider.getRates()
            .then((rateData) => {
                expect(rateData).to.be.ok;
                expect(rateData.rates).to.be.ok;
                done();
            });
    });

    it('should fetch rates from the external API if they are not in the store ', function (done) {
        let apiFetchSpy = chai.spy.on(RatesAPIMock, 'fetchRates');
        let storeFetchSpy = chai.spy.on(RatesStoreMock, 'fetchRates');
        let storeSetRatesFetchSpy = chai.spy.on(RatesStoreMock, 'setRates');
        RatesStoreMock.clear();

        let ratesProvider = new RatesProvider(RatesAPIMock, RatesStoreMock);
        ratesProvider.getRates()
            .then((rateData) => {

                expect(rateData).to.be.ok;
                expect(rateData.rates).to.be.ok;
                expect(storeFetchSpy).to.have.been.called.once;
                expect(apiFetchSpy).to.have.been.called.once;
                done();
            })
            .catch((err) => {
                console.log('ERR', err);
                done();
            });
    });

    it('should fetch rates from the store if they are available ', function (done) {
        let apiFetchSpy = chai.spy.on(RatesAPIMock, 'fetchRates');
        let storeFetchSpy = chai.spy.on(RatesStoreMock, 'fetchRates');

        RatesStoreMock.setRates({
                base: 'EUR',
                rates: {
                    'USD': 1.086,
                    'GBP': 0.896
                }
            })
            .then(() => {
                let ratesProvider = new RatesProvider(RatesAPIMock, RatesStoreMock);
                ratesProvider.getRates()
                    .then((rateData) => {
                        expect(rateData).to.be.ok;
                        expect(rateData.rates).to.be.ok;
                        expect(storeFetchSpy).to.have.been.called.once;
                        expect(apiFetchSpy).to.not.have.been.called();
                        done();
                    })
                    .catch((err) => {
                        console.log('ERR', err);
                        done();
                    });
            });


    });

    it('should write rates to the store after fetching from the API', function (done) {
        let apiFetchSpy = chai.spy.on(RatesAPIMock, 'fetchRates');
        let storeFetchSpy = chai.spy.on(RatesStoreMock, 'fetchRates');
        let storeSetRatesFetchSpy = chai.spy.on(RatesStoreMock, 'setRates');
        RatesStoreMock.clear();

        let ratesProvider = new RatesProvider(RatesAPIMock, RatesStoreMock);
        RatesStoreMock.setRates(null)
            .then(() => {
                ratesProvider.getRates()
                    .then((rateData) => {
                        expect(rateData).to.be.ok;
                        expect(rateData.rates).to.be.ok;
                        expect(storeFetchSpy).to.have.been.called.once;
                        expect(apiFetchSpy).to.have.been.called.once;
                        expect(storeSetRatesFetchSpy).to.have.been.called;
                        done();
                    })
                    .catch((err) => {
                        console.log('ERR', err);
                        done();
                    });
            });
    });
});
