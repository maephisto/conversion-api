/*global describe:false, it:false, beforeEach:false, afterEach:false*/

'use strict';


var kraken = require('kraken-js'),
    express = require('express'),
    path = require('path'),
    chai = require('chai'),
    expect = chai.expect,
    request = require('supertest');


describe('API specification', function () {

    var app, mock;


    beforeEach(function (done) {
        app = express();
        app.on('start', done);
        app.use(kraken({
            basedir: path.resolve(__dirname, '../..')
        }));

        mock = app.listen(1337);

    });


    afterEach(function (done) {
        mock.close(done);
    });


    it('should return a 400 when query string params are missing', function (done) {
        request(mock)
            .get('/api/v1/convert')
            .expect(400)
            .expect('Content-Type', "application/json; charset=utf-8")
            .end(function (err, res) {
                expect(res.body.message).to.equal('Bad request! Query string parameters are missing');
                done(err);
            });
    });

    it('should return a 400 when query string params are available but not the right ones', function (done) {
        request(mock)
            .get('/api/v1/convert?hey=there')
            .expect(400)
            .expect('Content-Type', "application/json; charset=utf-8")
            .end(function (err, res) {
                expect(res.body.message).to.equal(`Query string parameters 'from', 'to', 'amount' are mandatory`);
                done(err);
            });
    });

    it('should return a 400 when the amount is not a finite number', function (done) {
        request(mock)
            .get('/api/v1/convert?from=USD&to=GBP&amount=haha')
            .expect(400)
            .expect('Content-Type', "application/json; charset=utf-8")
            .end(function (err, res) {
                expect(res.body.message).to.equal(`The amount is not a number`);
                done(err);
            });
    });

    it('should return a 200 when the params are alright', function (done) {
        request(mock)
            .get('/api/v1/convert?from=USD&to=GBP&amount=100')
            .expect(200)
            .expect('Content-Type', "application/json; charset=utf-8")
            .end(function (err, res) {
                expect(res.body.convertedAmount).to.be.ok;
                done(err);
            });
    });

});
