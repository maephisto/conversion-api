/*global describe:false, it:false, beforeEach:false, afterEach:false*/

'use strict';


var kraken = require('kraken-js'),
    express = require('express'),
    path = require('path'),
    chai = require('chai'),
    expect = chai.expect,
    request = require('supertest');


describe('Index page specification', function () {

    var app, mock;


    beforeEach(function (done) {
        app = express();
        app.on('start', done);
        app.use(kraken({
            basedir: path.resolve(__dirname, '..')
        }));

        mock = app.listen(1337);

    });


    afterEach(function (done) {
        mock.close(done);
    });


    it('should return the index response as valid JSON', function (done) {
        request(mock)
            .get('/')
            .expect(400)
            .expect('Content-Type', "application/json; charset=utf-8")
            .end(function (err, res) {
                expect(res.body.message).to.equal('This is an API! Check the documentation at http://somedocumentation.com :)');
                done(err);
            });
    });

});
