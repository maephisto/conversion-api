'use strict';

module.exports = function (grunt) {
    // Load task
    grunt.loadNpmTasks('grunt-nodemon');

    // Options
    return {
        dev: {
            script: 'server.js'
        }
    };
};
