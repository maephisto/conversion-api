Currency Conversion API
===========

An API that uses ECB rates to convert currencies

### Usage

#### Running the app
```npm start```

### Running unit tests
```npm test```