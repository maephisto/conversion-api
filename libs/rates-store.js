'use strict';

const
    fs = require('fs'),
    path = require('path');

const 
    DIRNAME = '../data/',
    FILENAME = 'rates-data.json',
    FILE_NOT_EXISTING_ERR_CODE = 'ENOENT',
    ENCODING = 'utf8';

module.exports = {

    setRates: function (data) {
        return this.createDirectoryIfNotExisting(path.resolve(__dirname, DIRNAME))
            .then((result) => {
                return this.writeDataFile(path.resolve(__dirname, `${DIRNAME}${FILENAME}`), data);
            });
    },

    fetchRates: function () {

        return this.createDirectoryIfNotExisting(path.resolve(__dirname, DIRNAME))
            .then((result) => {
                return this.readDataFile(path.resolve(__dirname, `${DIRNAME}${FILENAME}`));
            });
    },

    writeDataFile: function (filename, data) {
        return new Promise((resolve, reject) => {
            fs.writeFile(path.resolve(__dirname, `${DIRNAME}${FILENAME}`), JSON.stringify(data), function (err) {
                if (err) {
                    return reject(err);
                }
                resolve(data);
            });
        });
    },

    readDataFile: function (filename) {

        return new Promise((resolve, reject) => {
            fs.readFile(filename, ENCODING, (err, data) => {
                if (err) {
                    resolve(false);
                }
                if (data && data.length > 0) {
                    data = JSON.parse(data);
                    resolve(data);
                } else {
                    resolve(false);
                }
            });
        });
    },

    createDirectoryIfNotExisting: function (directory) {
        return new Promise((resolve, reject) => {
            fs.stat(directory, function (err, stats) {

                if (stats) {
                    resolve(true);
                }
                if (err && err.code === FILE_NOT_EXISTING_ERR_CODE ) {
                    fs.mkdir(directory, resolve);
                } else {
                    reject(false);
                }
            });
        });
    }
};
