'use strict';

const
    request = require('request'),
    xml2js = require('xml2js'),
    ECB_API_URL = 'http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml';

module.exports = {
    fetchRates: function () {
        return new Promise((resolve, reject) => {
            request(ECB_API_URL, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    //parse the body
                    var parser = new xml2js.Parser({
                        trim: true,
                        normalizeTags: true,
                        mergeAttrs: true
                    });

                    parser.parseString(body, function (err, result) {
                        if (err) {
                            return reject(err);
                        }

                        try {
                            /**
                             * Go down through the xml response tree and get the rates
                             */
                            result = result['gesmes:envelope'].cube[0].cube[0].cube.reduce((ratesMap, entry) => {
                                ratesMap[entry.currency[0]] = entry.rate[0];
                                return ratesMap;
                            }, {});

                            resolve({
                                base: 'EUR',
                                rates: result
                            });

                        } catch (error) {
                            reject(new Error('Bad response from ECB server'));
                        }

                    });
                } else {
                    reject(new Error('Bad response from ECB server'));
                }
            });
        });
    }
};
