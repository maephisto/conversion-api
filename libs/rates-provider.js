'use strict';

var RatesProvider = function (RatesAPI, RatesStore) {
    if (!RatesAPI || !RatesStore) {
        throw new Error('Missing dependecies!');
    }
    this.ratesAPI = RatesAPI;
    this.ratesStore = RatesStore;
};
RatesProvider.prototype.getRates = function () {
    return this.ratesStore.fetchRates()
        .then((ratesData) => {
            if (ratesData) {
                return new Promise((resolve, reject) => { 
                    resolve(ratesData);
                });
            } else {
                return this.ratesAPI.fetchRates()
                .then((ratesData) => {
                    return this.ratesStore.setRates(ratesData);
                 });
            }
        })
};


module.exports = RatesProvider;