'use strict';

var CurrencyConverter = function (ratesProvider) {
    if (!ratesProvider) {
        throw new Error('A Rates Provider, we need, mhmm!');
    }

    this.ratesProvider = ratesProvider;
};

CurrencyConverter.prototype.convert = function (params) {
    return new Promise((resolve, reject) => {
        if (!params) {
            return reject(new Error('Conversion parameters are missing!'));
        }
        if (!params.from || !params.to || !params.amount) {
            return reject(new Error('Some of the conversion parameters are missing!'));
        }

        this.ratesProvider.getRates()
            .then((ratesData) => {
                if (ratesData && ratesData.rates) {
                    if (params.from === ratesData.base) {
                        let conversionRate = ratesData.rates[params.to];
                        resolve(this.convertAndRoundAmount(params.amount, conversionRate));
                    } else if (params.to === ratesData.base) {
                        let conversionRate = ratesData.rates[params.from];
                        resolve(this.convertAndRoundAmount(params.amount, 1 / conversionRate));
                    } else if (ratesData.rates[params.from] && ratesData.rates[params.to]) {
                        let baseFromCurrencyRate = ratesData.rates[params.from];
                        let baseToCurrencyRate = ratesData.rates[params.to];

                        let conversionRate = baseToCurrencyRate / baseFromCurrencyRate;
                        let convertedAmount = this.convertAndRoundAmount(params.amount, conversionRate);

                        resolve(convertedAmount);
                    } else {
                        return reject(new Error('One or both from/to currencies are invalid'));    
                    }
                } else {
                    return reject(new Error('Malformed rates data'));
                }
            })
            .catch((error) => {
                console.log('ERROR', error);
                reject(error);
            })
    });
};

CurrencyConverter.prototype.round = function (amount, precision = 4) {
    if (typeof amount === 'string') {
        throw new Error('Rounded amounts should be numbers, string provided!');
    }
    try {
        var factor = Math.pow(10, precision);
        return (Math.round(amount * factor) / factor);
    } catch (error) {
        return false;
    }
}

CurrencyConverter.prototype.convertAndRoundAmount = function (amount, rate) {
    if (!amount || !rate) {
        throw new Error('Missing amount or rate on conversion');
    }
    try {

        return this.round(amount * rate);
    } catch (error) {
        throw new Error('Error converting/rounding values');
    }
};


module.exports = CurrencyConverter;